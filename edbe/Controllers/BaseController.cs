﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using edbe.Models;

namespace edbe.Controllers
{
    public class BaseController : Controller
    {
        public eCommerceContext Entity;
        public BaseController()
        {
            Entity = new eCommerceContext();
        }

    }
}