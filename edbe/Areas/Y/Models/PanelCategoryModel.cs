﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace edbe.Areas.Y.Models
{
    public class PanelCategoryModel
    {


    }
    public class PanelCategory
    {
        public int Id { get; set; }
        [StringLength(30)]
        public string CategoryName { get; set; }
        public Nullable<int> ParentCategoryID { get; set; }        // 1-1 relations

        [StringLength(35)]
        public string Link { get; set; }
        public Nullable<int> IndexNo { get; set; }
        [StringLength(15)]
        public string Icon { get; set; }

    }
}