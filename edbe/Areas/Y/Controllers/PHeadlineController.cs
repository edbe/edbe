﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using edbe.Models;

namespace edbe.Areas.Y.Controllers
{
    [Authorize]
    public class PHeadlineController : BController
    {
        private eCommerceContext db = new eCommerceContext();

        // GET: Y/PHeadline
        public ActionResult Index()
        {
            return View(db.Headline.ToList());
        }

        // GET: Y/PHeadline/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Headline headline = db.Headline.Find(id);
            if (headline == null)
            {
                return HttpNotFound();
            }
            return View(headline);
        }

        // GET: Y/PHeadline/Create
        public ActionResult Create()
        {
            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CategoryID,Name,Description,PhotoUrl,IsActive")] Headline headline,HttpPostedFileBase Image)
        {
            if (ModelState.IsValid)
            {
                PhotoProcessor PhProces = new PhotoProcessor();

                PhProces.HeadlinePhotoUpload(headline, Image);

                db.Headline.Add(headline);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(headline);
        }

        // GET: Y/PHeadline/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Headline headline = db.Headline.Find(id);
            if (headline == null)
            {
                return HttpNotFound();
            }
            return View(headline);
        }

        // POST: Y/PHeadline/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CategoryID,Name,Description,PhotoUrl,IsActive")] Headline headline)
        {
            if (ModelState.IsValid)
            {
                db.Entry(headline).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(headline);
        }

        // GET: Y/PHeadline/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Headline headline = db.Headline.Find(id);
            if (headline == null)
            {
                return HttpNotFound();
            }
            return View(headline);
        }

        // POST: Y/PHeadline/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Headline headline = db.Headline.Find(id);
            db.Headline.Remove(headline);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
