﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using edbe.Models;

namespace edbe.Areas.Y.Controllers
{
    [Authorize]
    public class PContentController : BController
    {
        public ActionResult Index()
        {

            //app.min.js       ----- hasClass("expand")?($(this).removeClass("expand").addClass("collapse")
            // tabloların kapanıp açılmasını terse çevirdim !



            var contentCategories = Entity.ContentCategory.ToList().OrderBy(F=>F.IndexNo);
            return View(contentCategories);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                ViewBag.ContentCategories = Entity.ContentCategory.ToList();
                Content content = Entity.Content.FirstOrDefault(F => F.ContentCategoryID == id);
                if (content == null)
                {
                    return HttpNotFound();
                }
                return View(content);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                ViewBag.ContentCategoryID = Entity.ContentCategory.First(F=>F.SubCategoryID == id).Id;
                return View();
            }
            catch
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ContentCategoryID,Name,Title,Html")] Content content)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Entity.Content.First(F => F.ContentCategoryID == content.ContentCategoryID);
                    return RedirectToAction("Index");
                }
                catch
                {
                    Entity.Content.Add(content);
                    Entity.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ViewBag.ContentCategoryID = new SelectList(Entity.ContentCategory, "Id", "Name", content.ContentCategoryID);
            return View(content);
        }

        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Content content = Entity.Content.Find(id);
            if (content == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContentCategoryID = content.ContentCategoryID;
           
            return View(content);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,ContentCategoryID,Name,Title,Html")] Content content)
        {
            if (ModelState.IsValid)
            {
                Entity.Entry(content).State = EntityState.Modified;
                ContentCategory category  =  Entity.ContentCategory.Find(content.ContentCategoryID);
                category.Name = content.Name;
                Entity.SaveChanges();
                int id = content.ContentCategoryID.Value;
                return RedirectToAction("Details/"+id);
            }
            ViewBag.ContentCategoryID = content.ContentCategoryID;
            return View(content);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Entity.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
