﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using edbe.Models;

namespace edbe.Areas.Y.Controllers
{
    [Authorize]
    public class PShopController : BController
    {
        public ActionResult Index()
        {
            return View(Entity.Shop.ToList().OrderBy(F=>F.City));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = Entity.Shop.Find(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        // GET: Y/PanelShop/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Y/PanelShop/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,City,Address,Phone,Fax,Email")] Shop shop)
        {
            if (ModelState.IsValid)
            {
                Entity.Shop.Add(shop);
                Entity.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(shop);
        }

        // GET: Y/PanelShop/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = Entity.Shop.Find(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        // POST: Y/PanelShop/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,City,Address,Phone,Fax,Email")] Shop shop)
        {
            if (ModelState.IsValid)
            {
                Entity.Entry(shop).State = EntityState.Modified;
                Entity.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shop);
        }

        // GET: Y/PanelShop/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = Entity.Shop.Find(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            Danger("Bu mağazayı silmek istediğinize eminmisiniz ?", false);
            return View(shop);
        }

        // POST: Y/PanelShop/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {


            Shop shop = Entity.Shop.Find(id);
            Entity.Shop.Remove(shop);
            Entity.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Entity.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
