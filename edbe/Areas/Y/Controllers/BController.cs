﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using edbe.Areas.Y.Models;

namespace edbe.Areas.Y.Controllers
{
    public class BController : Controller
    {

        public edbe.Models.eCommerceContext Entity;

        public BController()
        {



            Entity = new edbe.Models.eCommerceContext();
            List <PanelCategory> Categories = Entity.PanelCategory.ToList();
            ViewBag.Categories = Categories;
           




        }


        public void Success(string message, bool dismissable = false)
        {
            AddAlert(Classes.Alerts.AlertStyles.Success, message, dismissable);
        }
        public void Information(string message, bool dismissable = false)
        {
            AddAlert(Classes.Alerts.AlertStyles.Information, message, dismissable);
        }

        public void Warning(string message, bool dismissable = false)
        {
            AddAlert(Classes.Alerts.AlertStyles.Warning, message, dismissable);
        }

        public void Danger(string message, bool dismissable = false)
        {
            AddAlert(Classes.Alerts.AlertStyles.Danger, message, dismissable);
        }

        private void AddAlert(string alertStyle, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Classes.Alerts.Alert.TempDataKey)
                ? (List<Classes.Alerts.Alert>)TempData[Classes.Alerts.Alert.TempDataKey]
                : new List<Classes.Alerts.Alert>();

            alerts.Add(new Classes.Alerts.Alert
            {
                AlertStyle = alertStyle,
                Message = message,
                Dismissable = dismissable
            });

            TempData[Classes.Alerts.Alert.TempDataKey] = alerts;
        }


    }
}