﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace edbe.Areas.Y.Controllers
{
    public class LoginController : BController
    {
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(edbe.Models.Admin Model)
        {
            try
            {
                if (Model.Name.Length < 3 && Model.Password.Length < 5)
                {
                    return Redirect("~/Y/login/login");
                }
                else
                {
                    var user =  Entity.Admin.First(F=>F.Name == Model.Name);

                    if (Model.Password == user.Password)
                    {
                        FormsAuthentication.SetAuthCookie(Model.Name, true);
                        return Redirect("/Y/Panel/Index");
                    }
                    else {
                        ViewBag.Message = "Kullanıcı Adınız veya Şifreniz Hatalı !";
                        return View();
                    }
                }
            }
            catch
            {
                ViewBag.Message = "Kullanıcı Adınız veya Şifreniz Hatalı !";
                return View();
            }
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Login");
        }
    }
}