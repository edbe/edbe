﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using edbe.Models;

namespace edbe.Areas.Y.Controllers
{
    [Authorize]
    public class PContentCategoryController : BController
    {
            
        // GET: Y/PContentCategory
        public ActionResult Index()
        {
            return View(Entity.ContentCategory.Where(F=>F.SubCategoryID == 0).ToList().OrderBy(F=>F.IndexNo));
        }
        public ActionResult SubContentCategories(int id)
        {
            if(Entity.ContentCategory.Where(F => F.SubCategoryID == id).ToList().Count == 0)
            {
                Warning("Kategoriye ait alt kategori bulunamadi", false);
                return RedirectToAction("index");
            }
            else
            {
                ViewBag.CategoryName = Entity.ContentCategory.First(F => F.Id == id).Name;
                return View(Entity.ContentCategory.Where(F => F.SubCategoryID == id).ToList());
            }
        }

        // GET: Y/PContentCategory/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContentCategory contentCategory = Entity.ContentCategory.Find(id);

            if (contentCategory == null)
            {
                return HttpNotFound();
            }

            return View(contentCategory);
        }

        // GET: Y/PContentCategory/Create
        public ActionResult Create(int id)
        {
             switch (id)
                {
                // id 1 ise alt kategori eklenecek  0 ise ana kategori eklenecek
                    case 1:
                        ViewBag.ParamId = id;

                    if (Entity.ContentCategory.Where(F=>F.SubCategoryID == 0).ToList().Count() == 0)
                        {
                        Warning("Önce Ana Kategori Ekleyiniz",false);
                        return RedirectToAction("index");
                        }

                        ViewBag.ContentCategories = Entity.ContentCategory.Where(f=>f.SubCategoryID == 0).ToList();
                        break;

                    case 0:
                        ViewBag.ParamId = id;
                        break;
                }
            return View();
        }
        
        // POST: Y/PContentCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,SubCategoryID,Description,IndexNo")] ContentCategory contentCategory)
        {
            if (ModelState.IsValid)
            {
                Entity.ContentCategory.Add(contentCategory);
                if (contentCategory.SubCategoryID != 0)
                {
                    Content newContent = new edbe.Models.Content();
                    newContent.ContentCategoryID = contentCategory.Id;
                    newContent.Name = contentCategory.Name;
                    Entity.Content.Add(newContent);
            
                }
                Entity.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contentCategory);
        }

        // GET: Y/PContentCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContentCategory contentCategory = Entity.ContentCategory.Find(id);
            if (contentCategory == null)
            {
                return HttpNotFound();
            }
            return View(contentCategory);
        }

        // POST: Y/PContentCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,SubCategoryID,Description,IndexNo")] ContentCategory contentCategory)
        {
            if (ModelState.IsValid)
            {
                Entity.Entry(contentCategory).State = EntityState.Modified;
                Entity.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contentCategory);
        }

        // GET: Y/PContentCategory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContentCategory contentCategory = Entity.ContentCategory.Find(id);
            if (contentCategory == null)
            {
                return HttpNotFound();
            }
            Danger("Bu kategoriyi silmek istediğinize eminmisiniz?", false);

            return View(contentCategory);
        }

        // POST: Y/PContentCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ContentCategory contentCategory = Entity.ContentCategory.Find(id);

            Entity.ContentCategory.Remove(contentCategory);
            //Entity.ContentCategory.RemoveRange(contentSubCategories);
            Entity.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Entity.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
