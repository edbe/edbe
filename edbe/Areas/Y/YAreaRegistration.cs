﻿using System.Web.Mvc;

namespace edbe.Areas.Y
{
    public class YAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Y";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
              "1",
              "Y/pcontentcategory/create/{id}",
              new { controller = "pcontentcategory", action = "create", id = UrlParameter.Optional }
          );

            context.MapRoute(
            "2",
            "Y/PContent/details/{id}",
            new { controller = "pcontent", action = "details", id = UrlParameter.Optional }
        );

            context.MapRoute(
                "Y_default",
                "Y/{controller}/{action}/{id}",
                new {controller="Index", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}