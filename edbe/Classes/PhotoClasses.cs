﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using edbe.Models;

namespace edbe
{
    public class PhotoProcessor : Controllers.BaseController
    {
        //PROCESSOR LAYER
        public void ProductPhotoUpload(ProductPhoto Model, HttpPostedFileBase Image)
        {
            if (Image.ContentLength > 0)
            {
                string FileName = Guid.NewGuid().ToString("N");
                //guid sınıfına ait newguid metodu benzersiz bir karakterler dizisi türetiyo -
                //9e15cf22838e4e0281fe6834ade3bb59B bunun gibi
                // okey sonuna Harf ekliyorum büyük orta küçük karışmasın diye

                var pathBig = Path.Combine("C:/Users/EDGE/Documents/Visual Studio 2015/Projects/Shop/Shop/Content/Images/UrunFoto/" + FileName + "B.jpeg");
                var pathMedium = Path.Combine("C:/Users/EDGE/Documents/Visual Studio 2015/Projects/Shop/Shop/Content/Images/UrunFoto/" + FileName + "M.jpeg");
                var pathSmall = Path.Combine("C:/Users/EDGE/Documents/Visual Studio 2015/Projects/Shop/Shop/Content/Images/UrunFoto/" + FileName + "S.jpeg");
                // yer belirliyo
                // akım üzerinden dosyayı okuma işlemi yapılıp yeni bir img dosyası oluşturuyo
                var filename = Path.GetFileName(Image.FileName);
                Image img = System.Drawing.Image.FromStream(Image.InputStream);
                
                //yeni imaj dosyalarımız
                Bitmap bmpBig = new Bitmap(1000, 1000);
                Bitmap bmpMedium = new Bitmap(320, 320);
                Bitmap bmpSmall = new Bitmap(170, 170);

                using (Graphics gr = Graphics.FromImage(bmpBig))
                {
                    gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    gr.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    gr.DrawImage(img, new Rectangle(0, 0, 1500, 1500));
                }

                using (Graphics gx = Graphics.FromImage(bmpMedium))
                {
                    gx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    gx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    gx.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    gx.DrawImage(img, new Rectangle(0, 0, 320, 320));
                }

                using (Graphics gx = Graphics.FromImage(bmpMedium))
                {
                    gx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    gx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    gx.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    gx.DrawImage(img, new Rectangle(0, 0, 170, 170));
                }
                bmpBig.Save(pathBig);
                bmpMedium.Save(pathMedium);
                bmpSmall.Save(pathSmall);

                ProductPhoto Ph = new ProductPhoto();
                Ph.BigPhotoUrl = "/content/images/ProductPhoto/" + FileName + "B.jpeg";
                Ph.MediumPhotoUrl = "/content/images/ProductPhoto/" + FileName + "M.jpeg";
                Ph.SmallPhotoUrl = "/content/images/ProductPhoto/" + FileName + "S.jpeg";
                Ph.ProductID = Model.Id;
                Entity.ProductPhoto.Add(Ph);
                Entity.SaveChanges();
            }
        }
        public void ProductPhotoDelete(int PhotoId)
        {
            ProductPhoto OnDelete = Entity.ProductPhoto.FirstOrDefault(f => f.Id == PhotoId);
            Entity.ProductPhoto.Remove(OnDelete);
            Entity.SaveChanges();

            FileInfo deleteFileB = new FileInfo(Server.MapPath(OnDelete.BigPhotoUrl));
            deleteFileB.Delete();
            FileInfo deleteFileM = new FileInfo(Server.MapPath(OnDelete.MediumPhotoUrl));
            deleteFileM.Delete();
            FileInfo deleteFileS = new FileInfo(Server.MapPath(OnDelete.SmallPhotoUrl));
            deleteFileS.Delete();
            // Ürüne ait fotoğraflar silinmeden önce veritabanındaki ürüne ait fotoğraf kayıtları silinmeli
            // aksi taktirde sayfa hataya düşebilir veya uygunsuz bir görünüm olabilir.
        }
        public void HeadlinePhotoUpload(Headline Model,HttpPostedFileBase Image)
        {
            if (Image.ContentLength > 0)
            {
                string FileName = Guid.NewGuid().ToString("N");
                //guid sınıfına ait newguid metodu benzersiz bir karakterler dizisi türetiyo -
                //9e15cf22838e4e0281fe6834ade3bb59B bunun gibi
                // okey sonuna Harf ekliyorum büyük orta küçük karışmasın diye

                var pathHeadline = Path.Combine(Server.MapPath("/Content/Images/UrunFoto/" + FileName + "H.jpeg"));
                // yer belirliyo
                // akım üzerinden dosyayı okuma işlemi yapılıp yeni bir img dosyası oluşturuyo
                var filename = Path.GetFileName(Image.FileName);
                Image img = System.Drawing.Image.FromStream(Image.InputStream);

                //yeni imaj dosyalarımız
                Bitmap bmpHeadline = new Bitmap(1000, 1000);

                using (Graphics gx = Graphics.FromImage(bmpHeadline))
                {
                    gx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    gx.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    gx.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    gx.DrawImage(img, new Rectangle(0, 0, 1000, 1000));
                }
                bmpHeadline.Save(pathHeadline);

                Headline Ph = new Headline();
                Ph.PhotoUrl = "/content/images/HeadlinePhotos/" + FileName + "H.jpeg";
                Ph.Name = Model.Name;
                Ph.Description = Model.Description;
                Ph.IsActive = true;
                Entity.Headline.Add(Ph);
                Entity.SaveChanges();

            }
        }
        public void HeadlinePhotoDelete(int PhotoId)
        {
            Headline OnDelete = Entity.Headline.FirstOrDefault(F => F.Id == PhotoId);
            Entity.Headline.Remove(OnDelete);
            Entity.SaveChanges();
            FileInfo deleteFileB = new FileInfo(Server.MapPath(OnDelete.PhotoUrl));
            deleteFileB.Delete();
        }
        public void HeadlinePhotoInActive(int PhotoId)
        {
            Headline Ph = Entity.Headline.FirstOrDefault(f => f.Id == PhotoId);
            //Ph.Status = false;
            Entity.SaveChanges();
        }
    }
}

