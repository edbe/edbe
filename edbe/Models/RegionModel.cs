﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class RegionModel { }
    public class Region
    {
        public Region()
        {
        }

        public int Id { get; set; }
        public int? CountryID { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }


        // Relations
        [ForeignKey("CountryID")]
        public virtual Country Country { get; set; }

        public virtual List<City> City { get; set; }

        /*
        Id	CountryID	RegionCode	RegionName
        1			
        2	TR	        TR.00	
        3	TR	        TR.AK	    Akdeniz Bölgesi
        4	TR	        TR.DA	    Doğu Anadolu Bölgesi
        5	TR	        TR.EG	    Ege Bölgesi
        6	TR	        TR.GDA	    Güney Doğu Anadolu Bölgesi
        7	TR	        TR.IC	    İç Anadolu Bölgesi
        8	TR	        TR.KR	    Karadeniz Bölgesi
        9	TR	        TR.MR	    Marmara Bölgesi
        */
    }
}