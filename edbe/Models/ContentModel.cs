﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class ContentModel
    {
    }

    public class Content
    {
        public int Id { get; set; }

        [Required]
        public int? ContentCategoryID { get; set; } // PanelCategory deki admin yönetilebilir de

        [Required(AllowEmptyStrings = false), StringLength(100)]
        public string Name { get; set; }

        [StringLength(250)]
        public string Title { get; set; }
        
        public string Html { get; set; }


        // Relations 
        [ForeignKey("ContentCategoryID")]
        public virtual ContentCategory ContentCategory { get; set; }


    }
}