﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class ReminderModel { }
    public class Reminder
    {
        public Reminder()
        {
        }

        public int Id { get; set; }
        public int CustomerID { get; set; }
        public int StockID { get; set; }
        public bool Email { get; set; }
        public bool CanSendMail { get; set; }
        public bool CanSendSms { get; set; }
        public System.DateTime CreateDate { get; set; }

        // Relations
        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("StockID")]
        public virtual Stock Stock { get; set; }


        /*
        Id	CustomerID	StockID	Email	CanSendMail	CanSendSms	CreateDate	Status
        1	3177	    779	    0	    1	        0	        2013-03-15 	1
        2	3183	    1416	0	    1	        0	        2013-03-16 	1
        */
    }
}
