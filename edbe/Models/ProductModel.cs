﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class ProductModel { }
    public class Product
    {
        public Product()
        {
        }

        public int Id { get; set; }
        public int SupplierID { get; set; }
        public int CategoryID { get; set; }
        public string ProductName { get; set; }
        public string ShortDescription { get; set; }
        public string ListDescription { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string Description3 { get; set; }
        public string Description4 { get; set; }
        public string Description5 { get; set; }
        public int VatID { get; set; }
        public string ProductCode { get; set; }
        public decimal OldPrice { get; set; }
        public bool IsDiscounted { get; set; }
        public decimal SalePrice { get; set; }
        public string AttributeName { get; set; }
        public string Unit { get; set; }
        public bool IsNew { get; set; }
        public bool IsSpecialDay { get; set; }
        public int DisplayOrder { get; set; }
        public System.DateTime PublishDate { get; set; }
        public System.DateTime CreateDare { get; set; }
        public int TypeID { get; set; }
        public int Status { get; set; }


        // Relations
        [ForeignKey("SupplierID")]
        public virtual Supplier Supplier { get; set; }

        [ForeignKey("CategoryID")]
        public virtual ProductCategory ProductCategory { get; set; }

        public virtual List<ProductComment> ProductComment { get; set; }
        public virtual List<ProductPhoto> ProductPhoto { get; set; }
        public virtual List<ProductRelation> ProductRelations { get; set; }
        public virtual List<ProductSuggestion> ProductSuggestion { get; set; }

        /*
        ProductID	BrandID	CategoryID	SubCategoryID	ProductName	        ShortDescription	ListDescription	Description1    Description2	Description3	Description4	Description5    VatID	ProductCode	    OldPrice	IsDiscounted	SalePrice	AttributeName	Unit	IsNew	IsSpecialDay	DisplayOrder	PublishDate	    CreateDare	TypeID	Status
        100	        79	    74	        148	            Fred Perry Gömlek	                                                                                                                   2	    123FPKGMG1725	275,00	    1	            220,00	    NULL	        AD	    0	    0	            97	            2013-02-14  	2013-02-14  1	    1
        */
    }
}
