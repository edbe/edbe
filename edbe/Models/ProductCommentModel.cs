﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace edbe.Models
{
    public class ProductCommentModel { }
    public class ProductComment
    {
        public ProductComment()
        {
        }

        public int Id { get; set; }
        public int ProductID { get; set; }
        public int CustomerID { get; set; }
        public int Vote { get; set; }
        public int VoteCount { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreateDate { get; set; }



        // Relations
        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }

        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }
    }
}
