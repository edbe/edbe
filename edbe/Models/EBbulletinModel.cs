﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace edbe.Models
{
    public class EBbulletinModel { }
    public class EBbulletin
    {
        public EBbulletin()
        {
        }

        public int Id { get; set; }

        public int? CustomerID { get; set; } // Nullable çünkü müşteri olmayanların verileride bu tabloda olabilir

        [Required(AllowEmptyStrings =false,ErrorMessage ="Eposta zorunlu"), DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DefaultValue(1)]
        public bool IsActive { get; set; }

        /*
        Id	Email
        1	edge@hotmail.com
        2	berkay@gmail.com
        */
    }
}
