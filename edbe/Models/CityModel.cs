﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class CityModel { }
    public class City
    {
        public City()
        {
        }
    
        public int Id { get; set; }

        public int? RegionID { get; set; }

        public string CityCode { get; set; }

        public int? LicencePlateCode { get; set; }

        [Required, StringLength(maximumLength: 100, ErrorMessage = "Şehir Adı")]
        public string CityName { get; set; }


        // Relations
        [ForeignKey("RegionID")]
        public virtual Region Region { get; set; }

        public virtual List<District> District { get; set; }
        public virtual List<Order> Order { get; set; }
        public virtual List<BillingAddress> BillingAddress { get; set; }
        public virtual List<DeliveryAddress> DeliveryAddress { get; set; }

        /*
        Id	RegionCode	CityCode	LicencePlateCode	CityName	DisplayOrder
        0	TR.00	    TR.00	    0		            0           0
        1	TR.AK	    TR.01	    1	                Adana	    1
        2	TR.GDA	    TR.02	    2	                Adıyaman	2
        3	TR.EG	    TR.03	    3	                Afyon	    3
        */
    }
}