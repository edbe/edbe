﻿using System;
using System.ComponentModel.DataAnnotations;

namespace edbe.Models
{
    public class LogModel { }
    public class Log
    {
        public Log()
        {
        }

        public int Id { get; set; }

        [Required, StringLength(150)]
        public string Logger { get; set; }
        public string Thread { get; set; }

        [Required, StringLength(50)]
        public string Level { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public System.DateTime Date { get; set; }

        /*
        Id	Logger	        Thread	Level	Message	Exception	            Date
        1	edge.Admin	    1	    INFO	For object serializer		    2013-03-03
        2	edge.Admin  	1	    INFO	For ThreadExecutor		        2013-03-03
        */
    }
}
