﻿using System;
using System.ComponentModel.DataAnnotations;

namespace edbe.Models
{
    public class SenderMailInfoModel { }
    public class SenderMailInfo
    {
        public SenderMailInfo()
        {
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string SenderMail { get; set; }
        public string SenderFirstLastName { get; set; }
        public string MailAccount { get; set; }
        public string MailPassword { get; set; }
        public string Server { get; set; }
        public int Port { get; set; }
        public bool SSL { get; set; }
        public string ReceiverMail { get; set; }
        public string CCs { get; set; }
        public string BCCs { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime UpdateDate { get; set; }

        /*

    Id	Title	                            Subject	Body	            SenderMail	    SenderFirstLastName	MailAccount	MailPassword	Server	        Port	SSL	ReceiverMail	CCs	BCCs	CreateDate	UpdateDate	Status
    1	Şifremi Unuttum	Şifre Hatırlatma	HTML                        shop@shop.com	shop.com	        bilsarweb	qwerty	        smtp.abc.com	587	    0	                            2012-10-21  2012-10-21	1
    2	Üyelik İptali	                    Üyeliğiniz İptal Edilmiştir	
    */
    }
}
