﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class CartModel
    {

    }
    public class Cart
    {
        public Cart()
        {
        }

        public int Id { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="Müşteri zorunlu")]
        public int CustomerID { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage = "Ürün detay seçiniz")]
        public int StockID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Miktar giriniz")]
        public int Qty { get; set; }

        [Required(AllowEmptyStrings = false,ErrorMessage = "Tutar boş olamaz")]
        public decimal UnitAmount { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Tutar boş olamaz")]
        public decimal TotalAmount { get; set; }

        public System.DateTime AddingDate { get; set; }


        // Relations
        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("StockID")]
        public virtual Stock Stock { get; set; }


        /*
        Id	CustomerID	StockID	Qty	UnitAmount	TotalAmount	AddingDate
        197	1707	    1386	1	50,00	    50,00	    2013-03-25
        198	1707	    2024	1	87,50	    87,50	    2013-03-25
        200	1707	    2019	1	87,50	    87,50	    2013-03-25
        */
    }
}
