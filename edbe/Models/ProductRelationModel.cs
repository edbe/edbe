﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class ProductRelationModel { }
    public class ProductRelation
    {
        public ProductRelation()
        {
        }

        public int Id { get; set; }
        public int ProductID { get; set; }
        public int RelationID { get; set; }


        // Relations
        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }

        [ForeignKey("RelationID")] // selft relations
        public virtual ProductRelation Relation { get; set; }



    }
}
