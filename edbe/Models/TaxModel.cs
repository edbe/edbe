﻿using System;
using System.ComponentModel.DataAnnotations;

namespace edbe.Models
{
    public class TaxModel { }
    public class Tax
    {
        public Tax()
        {
        }

        public int Id { get; set; }
        public string TaxCode { get; set; }
        public int TaxRate { get; set; }

        /*
        Id	TaxRate
        1	18
        2	8
        3	0
        */
    }
}
