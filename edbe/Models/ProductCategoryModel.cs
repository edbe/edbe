﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace edbe.Models
{
    public class ProductCategoryModel
    {
    }
    public class ProductCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }

        
        // Relations
        public virtual List<Product> Product { get; set; }
    }
}