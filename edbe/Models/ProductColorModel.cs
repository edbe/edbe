﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace edbe.Models
{
    public class ProductColorModel
    {
    }

    public class ProductColor
    {
        public int Id { get; set; }
        public string ColorCode { get; set; }
        public string Name { get; set; }
    }
}