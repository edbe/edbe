﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class ProductSuggestionModel { }
    public class ProductSuggestion
    {
        public ProductSuggestion()
        {
        }

        public int Id { get; set; }
        public int ProductID { get; set; }
        public int SuggestionID { get; set; }


        // Relation
        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }

        [ForeignKey("SuggestionID")]
        public virtual ProductSuggestion Suggestion { get; set; }
        /*
        Id	ProductID	SuggestionID
        3	4	        3
        4	4	        2
        5	4	        1
        6	6	        5
        */
    }
}
