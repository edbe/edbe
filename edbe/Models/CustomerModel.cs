﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Security;

namespace edbe.Models
{
    public class CustomerModel { }
    public class Customer
    {
        public Customer()
        {
        }

        public int Id { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="e-posta zorunlu")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Lütfen e-posta adresinizi geçerli bir formatta giriniz.")]
        public string Email { get; set; }

        [MembershipPassword(
        MinRequiredPasswordLength = 8,
        ErrorMessage = "En az 8 karakter kullanın",
        MinRequiredNonAlphanumericCharacters = 1,
        MinNonAlphanumericCharactersError = "En az 1 özel karakter kullanın")]
        public string Password { get; set; }

        [Required(AllowEmptyStrings =false, ErrorMessage = "Ad Soyad zorunlu"),
        StringLength(maximumLength:75,MinimumLength =2, ErrorMessage = "Ad Soyad hatalı")]
        public string FistLastName { get; set; }

        public string CardNumber { get; set; }
        
        public DateTime? BirthDate { get; set; }

        [StringLength(5)]
        public string Gender { get; set; }

        [StringLength(250)]
        public string AddressDescription { get; set; }

        [StringLength(500)]
        public string Address { get; set; }

        [StringLength(10)]
        public string PostCode { get; set; }

        public int? CountryID { get; set; }
        public int? RegionID { get; set; }
        public int? CityID { get; set; }
        public int? DistrictID { get; set; }

        [Phone]
        public string Mobile { get; set; }

        [Phone]
        public string Phone { get; set; }


        [StringLength(100)]
        public string QuarterName { get; set; }

        [Required, DefaultValue(0)]
        public bool IsConfirmed { get; set; }

        [StringLength(50)]
        public string CustomerCode { get; set; }

        public int TypeID { get; set; }
        public System.DateTime RegisterDate { get; set; }

        [StringLength(50)]
        public string ShipFirstName { get; set; } // Gidecek kişinin adı

        [StringLength(50)]
        public string ShipLastName { get; set; } // Gidecek soy adı
        public int Status { get; set; }


        // Relations
        [ForeignKey("CountryID")]
        public virtual Country Country { get; set; }

        [ForeignKey("RegionID")]
        public virtual Region Region { get; set; }

        [ForeignKey("CityID")]
        public virtual City City { get; set; }

        [ForeignKey("DistrictID")]
        public virtual District District { get; set; }


        public virtual List<BillingAddress> BillingAddress { get; set; }
        public virtual List<DeliveryAddress> DeliveryAddress { get; set; }
        public virtual List<Cart> Cart { get; set; }
        public virtual List<ProductComment> ProductComment { get; set; }



        /*
        Id	Email	        Password	    FistLastName	CardNumber	BirthDate	Gender	AddressDescription	Address	            PostCode	CityID	CountryID	Mobile	    Phone	    DistrictID	QuarterName	IsConfirmed	CustomerCode	TypeID	RegisterDate	Status	ShipFirstName	ShipLastName
        2	aaa@yahoo.com	123456789   	Ayşe Fatma	    NULL		1950-01-01  Kadın   Teslimat adresi	    Bebek / Besiktas  	34342	    34	    NULL	                2122636157	NULL	    Mahalle	    1           199800095484	1	    2013-03-13  	1	    Ayşe            Fatma
        */
    }
}
