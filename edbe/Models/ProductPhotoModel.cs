﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class ProductPhotoModel
    {
    }
    public class ProductPhoto
    {
        public int Id { get; set; }

        public int ProductID { get; set; }

        [StringLength(200)]
        public string BigPhotoUrl { get; set; }

        [StringLength(200)]
        public string MediumPhotoUrl { get; set; }

        [StringLength(200)]
        public string SmallPhotoUrl { get; set; }

        public int DisplayOrder { get; set; } // Resimlerin Sırası hangisi önde gözükecek ? 

        public DateTime CreateDate { get; set; }


        // Relations
        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }

        /*
        Id	ProductID	ColorID	    SmallPhotoUrl	                                                MediumPhotoUrl	BigPhotoUrl     IsDefault	DisplayOrder	CreateDate	Status
        17	1	        80	        /Data/ProductPhoto/1/5c2afe11-65c1-4930-81dc-a00ea366810f.jpg	                                0	        1	    2012-10-16  1
*/
    }
}
 