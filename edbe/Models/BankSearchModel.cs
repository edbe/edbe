﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class BankSearchModel
    {
    }

    public class BankSearch
    {
        public BankSearch()
        {
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Banka zorunlu")]
        public int BankID { get; set; } // Banka id si

        [Required(ErrorMessage = "Şube adı zorunlu")]
        public int BankNumber { get; set; } // no şube no gibi


        // Relations
        [ForeignKey("BankID")]
        public virtual Bank Bank { get; set; }

        /*
        Id	BankID	BankNumber
        213	114	    605036
        214	114	    540669
        215	114	    520922

        */
    }
}
