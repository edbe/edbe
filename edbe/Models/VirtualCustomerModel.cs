﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace edbe.Models
{

    public class VirtualCustomerModel { }
    public class VirtualCustomer
    {
        public VirtualCustomer()
        {
            this.VirtualCart = new List<VirtualCart>();
        }

        public int Id { get; set; }
        public System.Guid VirtualCustomerGUID { get; set; }
        public string VirtualCustomerName { get; set; }

        public virtual List<VirtualCart> VirtualCart { get; set; }

        /*
        Id	VirtualCustomerGUID     VirtualCustomerName
        1	MSGUID ****             1
        2	                        2
        3	                        3
        4	                        4
        5	                        5
        */
    }
}
