﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class AdminModel
    {
    }

    public class Admin
    {
        public Admin()
        {
        }

        public int Id { get; set; }

        [Required(ErrorMessage ="Admin kullanıcı adı zorunlu"), StringLength(100)]
        public string Name { get; set; } // Admin kullanıcı adı ve şifresi

        [Required(ErrorMessage = "Admin kullanıcı şifresi zorunlu"), StringLength(75)]
        public string Password { get; set; }



        // Relations
        public  virtual List<AdminPermit> AdminPermit { get; set; }

        /* Örnek

        Id	AdminName	    Password
        1	administrator	123456
        3	operasyon	    1234

        */

    }
}
