﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class HeadlineModel
    {
    }

    public class Headline
    {
        public Headline()
        {

        }
        public int Id { get; set; }

        public int? CategoryID { get; set; }

        [StringLength(150)]
        public string Name { get; set; }

        [StringLength(350)]
        public string Description { get; set; }

        [StringLength(250)]
        public string PhotoUrl { get; set; }

        [DefaultValue(true)]
        public bool IsActive { get; set; }



    }
}