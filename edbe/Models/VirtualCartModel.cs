﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class VirtualCartModel { }
    public class VirtualCart
    {
        public VirtualCart()
        {

        }

        public int Id { get; set; }
        public int VirtualCustomerID { get; set; }
        public int StockID { get; set; }
        public int Qty { get; set; }
        public decimal UnitAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public System.DateTime AddingDate { get; set; }


        // Relations
        [ForeignKey("VirtualCustomerID")]
        public virtual VirtualCustomer VirtualCustomer { get; set; }

        [ForeignKey("StockID")]
        public virtual Stock Stock { get; set; }

        /*
        Id	CustomerID	StockID	Qty	UnitAmount	TotalAmount	AddingDate
        2	00000003	1278	1	115,00	    115,00	    2013-03-14 13:43:18.553
        4	00000005	2050	1	289,00	    289,00	    2013-03-14 19:05:38.330
        */
    }
}
