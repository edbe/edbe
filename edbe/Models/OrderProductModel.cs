﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class OrderProductModel { }
    public class OrderProduct
    {
        public OrderProduct()
        {
        }

        public int Id { get; set; }
        public int OrderID { get; set; }
        public int StockID { get; set; }
        public int Qty { get; set; }
        public int VatRate { get; set; }
        
        // Fiyat bilgileri
        public decimal UnitPrice { get; set; }
        public decimal UnitPriceVI { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalAmountVI { get; set; }
        public decimal UnitPriceVat { get; set; }
        public decimal TotalTaxBase { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TotalNetAmount { get; set; }
        // Fiyat bilgileri


        // Relations
        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }

        [ForeignKey("StockID")]
        public virtual Stock Stock { get; set; }

        /*
        Id	OrderID	CustomerID	StockID	Qty	VatRate	UnitPrice	TotalAmount	UnitPriceVI	TotalAmountVI	UnitPriceVat	TotalTaxBase	IsReturn	ReturnQty	ReturnDate	ReturnReason	DiscountAmount
        1	1	   	3135	    1912	1	8	    0,93	    0,93	    1,00	    1,00	        0,07	        0,07	        NULL	    NULL	    NULL	    NULL	        0,00
        */
    }
}
