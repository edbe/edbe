﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace edbe.Models
{
    public class PermitModel { }
    public class Permit
    {
        public Permit()
        {
        }

        public int Id { get; set; }

        [StringLength(100)]
        public string PermitName { get; set; }
        public int PermitCode { get; set; }


        //Relations
        public virtual List<AdminPermit> AdminPermit { get; set; }


        /*
        Id	PermitName	                PermitCode
        6	Bekleyen İade Talepleri	    6
        7	İade Edilen Siparişler	    7
        */
    }
}
