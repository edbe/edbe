﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class CountryModel { }
    public class Country
    {
        public Country()
        {
        }

        public int Id { get; set; }

        [Required(AllowEmptyStrings =false, ErrorMessage = "Ülke kodu zorunlu"), StringLength(maximumLength: 10)]
        public string CountryCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Ülke adı zorunlu"), StringLength(maximumLength: 100)]
        public string CountryName { get; set; }


        // Rleations
        public virtual List<Region> Region { get; set; }



        /*
        Id	CountryCode	CountryName
        1		
        2	AD	        Andora
        3	AE	        Birleşik Arap Emirlikleri
        4	AF	        Afganistan
        */
    }
}
