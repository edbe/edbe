﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class BillingAddressModel
    {
    }
    public class BillingAddress
    {
        public BillingAddress()
        {
        }

        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Müşteri zorunlu")]
        public int CustomerID { get; set; }

        [Required(ErrorMessage = "Adres başlığı zorunlu"), StringLength(100)]
        public string AddressTitle { get; set; }

        [Required(ErrorMessage = "Ad Soyad zorunlu"), StringLength(100)]
        public string FirstLastName { get; set; }

        [StringLength(150)]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Adres zorunlu"), StringLength(100)]
        public string Address { get; set; }

        [Required(AllowEmptyStrings =false, ErrorMessage = "Şehir zorunlu")]
        public int CityID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "İlçe zorunlu")]
        public int DistrictID { get; set; }

        [StringLength(50)]
        public string QuarterName { get; set; }

        [StringLength(10)]
        public string PostCode { get; set; }

        [Phone]
        public string Phone { get; set; }

        [Required,Phone]
        public string Mobile { get; set; }

        [StringLength(75)]
        public string TaxOffice { get; set; }

        [StringLength(15)]
        public string TaxNumber { get; set; }


        // Relations
        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("CityID")]
        public virtual City City { get; set; }

        [ForeignKey("DistrictID")]
        public virtual District District { get; set; }

    }

}
