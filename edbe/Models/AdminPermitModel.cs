﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class AdminPermitModel
    {
    }

    public class AdminPermit
    {
        public AdminPermit()
        {
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Admin kodu zorunlu")]
        public int AdminID { get; set; } // Yetki Id si

        [Required(ErrorMessage = "Yetki kodu zorunlu")]
        public int PermitID { get; set; } // Yetki Id si

        [Required(ErrorMessage = "Yetki adı zorunlu"), StringLength(100)]
        public string Name { get; set; } // Yetki ismi        



        // Relations
        [ForeignKey("AdminID")]
        public virtual Admin Admin { get; set; }

        [ForeignKey("PermitID")]
        public virtual Permit Permit { get; set; }


        /* Örnek 
        Id	PermitID	PermitName	PermitCode
        7	1	        İadeler	    7
        8	1	        Ürünler	    8
        */
    }


}
