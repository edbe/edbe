﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class PaymentInfoModel { }
    public class PaymentInfo
    {
        public PaymentInfo()
        {
        }

        public int Id { get; set; }
        public int OrderID { get; set; }
        public int BankID { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }

        public System.DateTime DueDate { get; set; }


        // Relations 
        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }

        [ForeignKey("BankID")]
        public virtual Bank Bank { get; set; }


        /*
        Id	OrderID	BankID	CurrAccID	OrderNumber	    OrderDate	DueDate	    Amount
        6	13	    116	    1543	    000000000000013	2013-03-15  2013-03-15  112,50
        7	14	    119 	1544	    000000000000014	2013-03-15  2013-03-15  144,00
        */
    }
}
