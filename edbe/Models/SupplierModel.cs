﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace edbe.Models
{
    public class SupplierModel
    {
    }
    public class Supplier
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="İsim zorunlu"), StringLength(100)]
        public string Name { get; set; }

        public virtual List<Product> Order { get; set; }
    }
}