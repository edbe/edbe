﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class DeliveryAddressModel { }
    public class DeliveryAddress
    {
        public DeliveryAddress()
        {
        }

        public int Id { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="Müşteri zorunlu")]
        public int CustomerID { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="Adres başlığı zorunlu"), StringLength(100)]
        public string AddressTitle { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Adres başlığı zorunlu"), StringLength(100)]
        public string FirstLastName { get; set; }

        [StringLength(150)]
        public string CompanyName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Adres zorunlu"), StringLength(250)]
        public string Address { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Şehir zorunlu")]
        public int CityID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "ilçe zorunlu")]
        public int DistrictID { get; set; }

        [StringLength(75)]
        public string QuarterName { get; set; }

        [StringLength(25)]
        public string PostCode { get; set; }

        [StringLength(20),Phone]
        public string Phone { get; set; }

        [StringLength(20),Phone]
        public string Mobile { get; set; }



        // Relations
        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("CityID")]
        public virtual City City { get; set; }

        [ForeignKey("DistrictID")]
        public virtual District District { get; set; }


        /*
        Id	CustomerID		AddressTitle	FirstLastName	CompanyName	Address	CityID	DistrictID	QuarterName	PostCode	Phone	Mobile
        4	7	            Ev	            Ayşe Fatma                  Eyüp    34                              34370		        +9053521141111
        */
    }
}
