﻿using System.Data.Entity;
using edbe.Areas.Y.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace edbe.Models
{
    public class eCommerceContext : DbContext
    {
        public eCommerceContext():base("ECOMMERCE")
        {
        }

        public DbSet<Admin> Admin { get; set; }
        public DbSet<AdminPermit> AdminPermit { get; set; }
        public DbSet<BankSearch> BankSearche { get; set; }
        public DbSet<BillingAddress> BillingAddress { get; set; }
        public DbSet<Cargo> Cargo { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<ContentCategory> ContentCategory { get; set; }
        public DbSet<Content> Content { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<DeliveryAddress> DeliveryAddress { get; set; }
        public DbSet<District> District { get; set; }
        public DbSet<EBbulletin> EBbulletin { get; set; }
        public DbSet<Headline> Headline { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderProduct> OrderProduct { get; set; }
        public DbSet<PaymentInfo> PaymentInfo { get; set; }
        public DbSet<Permit> Permit { get; set; }
        public DbSet<ProductCategory> ProductCategory { get; set; }
        public DbSet<ProductColor> ProductColor { get; set; }
        public DbSet<ProductComment> ProductComment { get; set; }
        public DbSet<ProductDim> ProductDim { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductPhoto> ProductPhoto { get; set; }
        public DbSet<ProductRelation> ProductRelation { get; set; }
        public DbSet<ProductSuggestion> ProductSuggestion { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<Reminder> Reminder { get; set; }
        public DbSet<SenderMailInfo> SenderMailInfo { get; set; }
        public DbSet<Shop> Shop { get; set; }
        public DbSet<Stock> Stock { get; set; }
        public DbSet<Tax> Tax { get; set; }
        public DbSet<VirtualCart> VirtualCart { get; set; }
        public DbSet<VirtualCustomer> VirtualCustomer { get; set; }


        // PANEL MODELS

        public DbSet<PanelCategory> PanelCategory { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>(); // MSSQL deki cascade deletelerin tümünü kaldırmak için
        }
    }
}