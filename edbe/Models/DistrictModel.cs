﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class DistrictModel { }
    public class District
    {
        public District()
        {
        }

        public int Id { get; set; }
        public int? CityID { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }


        // Relations
        [ForeignKey("CityID")]
        public virtual City City { get; set; }

        public virtual List<BillingAddress> BillingAddress { get; set; }
        public virtual List<DeliveryAddress> DeliveryAddress { get; set; }


        /*
        Id	DistrictCode	CityCode	CityID	DistrictName
        0	NULL	        NULL	    0	    Diğer
        1	TR.00114	    TR.01	    1	    ALADAĞ
        */
    }
}
