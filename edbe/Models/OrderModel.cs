﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edbe.Models
{
    public class OrderModel { }
    public class Order
    {
        public Order()
        {
        }

        public int Id { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="Sipariş no zorunlu"), StringLength(25)]
        public string OrderNumber { get; set; }

        [Required]
        public int CustomerID { get; set; }

        public int CargoID { get; set; }
        public string CargoTrackingNo { get; set; }
        public string CargoStatus { get; set; }
        public int OrderDeliveryAddressID { get; set; }
        public int OrderBillingAddressID { get; set; }

        // Parasal işlemler
        [Required(AllowEmptyStrings =true)]
        public decimal Discount { get; set; }

        [Required(AllowEmptyStrings = true)]
        public decimal CargoAmount { get; set; }

        [Required(AllowEmptyStrings = true)]
        public decimal GiftPackAmount { get; set; }

        [Required(AllowEmptyStrings = true)]
        public decimal CommissionAmount { get; set; }

        [Required(AllowEmptyStrings = true)]
        public decimal PromotionCodeAmount { get; set; }
        // Parasal işlemler

        public string OrderNote { get; set; }

        public bool IsGiftPack { get; set; }
        public bool IsDelivered { get; set; }
        public bool? IsCancel { get; set; }
        public bool? IsReturn { get; set; }

        public DateTime? CargoDeliveryDate { get; set; }
        public System.DateTime OrderDate { get; set; }

        public int Status { get; set; }


        
        // Relations
        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("CargoID")]
        public virtual Cargo Cargo { get; set; }

        public virtual List<OrderProduct> OrderProduct { get; set; }



        /*
        Id	OrderNumber 	IntegrationID	CustomerID	Installment	BankID	CargoID	CargoTrackingNo	OrderDeliveryAddressID	OrderBillingAddressID	CargoStatus	    CanSendMail	PaymentPlan	    CargoAmount	GiftPackAmount	VatAmount	CommissionAmount	PaidAmount	PointAmount	PromotionCodeAmount	DiscountAmount	WonPoint	IsDelivered	CargoDeliveryDate	IsCancel	IsReturn	IsGiftPack	ProcessNumber	OrderNote	TypeID	OrderDate	Status
        1	000000000000001	NULL	        3135	    1	        116	    113	    NULL	        1	                    1	                    İptal Edildi	1	        1 x 1,00 TL	    0,00	    0,00	        0,07	    0,00	            1,00	    0,00	    0,00	            0,00	        0,00	    0	        NULL			    0	        0           1	                                            2013-03-13 	4
        */
    }
}
