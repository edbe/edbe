﻿using System;
using System.ComponentModel.DataAnnotations;

namespace edbe.Models
{
    public class StockModel { }
    public class Stock
    {
        public Stock()
        {
        }

        public int Id{ get; set; }
        public string StockCode { get; set; }

        public int ProductID { get; set; }

        public int ColorID { get; set; }

        public int DimID { get; set; }


        public string Barcode { get; set; }
        public double StockQty { get; set; }
        public int Status { get; set; }



        /*
        ProductID	DimID	ColorID	DimCode	ColorCode	StockCode	    Barcode	        StockQty	Status
        54	        94	    208 	L	    3354	    8699023852073	8699023852073	0	        1
        54	        93	    208	    M	    3354	    8699023852080	8699023852080	0	        1
        54	        92	    208	    S	    3354	    8699023852097	8699023852097	0	        1
        54	        95	    208	    XL	    3354	    8699023852103	8699023852103	0	        1
        54	        96  	208	    XXL	    3354	    8699023852110	8699023852110	0	        1
        54	        94  	85	    L	    1600	    8699023851878	8699023851878	0	        1
        54	        93  	85	    M	    1600	    8699023851885	8699023851885	0	        1
        54	        92  	85	    S	    1600	    8699023851892	8699023851892	0	        1
        54	        95  	85  	XL	    1600	    8699023851908	8699023851908	0	        1
        54	        96  	85	    XXL	    1600	    8699023851915	8699023851915	0	        1
        54	        94  	178 	L	    0959	    8699985085649	8699985085649	0	        1
        54	        92  	178 	S	    0959	    8699985085663	8699985085663	0	        1
        54	        95	    178 	XL	    0959	    8699985085670	8699985085670	0	        1
        54	        94  	177 	L	    0451	    8699023852578	8699023852578	0	        1
        54	        93	    177 	M	    0451	    8699023852585	8699023852585	0	        1   
        54	        92	    177 	S	    0451	    8699023852592	8699023852592	0	        1
        54	        95	    177 	XL	    0451	    8699023852608	8699023852608	2	        1
        54	        96	    177 	XXL	    0451	    8699023852615	8699023852615	0	        1
        */
    }
}
