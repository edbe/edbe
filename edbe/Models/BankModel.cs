﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace edbe.Models
{
    public class BankModel
    {
    }
    public class Bank
    {
        public Bank()
        {
            this.BankSearch = new List<BankSearch>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Banka adı zorunlu"), StringLength(100)]
        public string Name { get; set; }



        // Relations
        public virtual List<PaymentInfo> PaymentInfo { get; set; }
        public virtual List<BankSearch> BankSearch { get; set; }
    }
}