﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace edbe.Models
{
    public class ProductDimModel
    {
    }
    public class ProductDim
    {
        public int Id { get; set; }
        public string DimCode { get; set; }
        public string Name { get; set; }
    }
}