﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace edbe.Models
{
    public class ShopModel
    {
    }


    public class Shop
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="Mağaza adı zorunlu"),StringLength(100)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Şehir adı zorunlu"), StringLength(50)]
        public string City { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Adres adı zorunlu"), StringLength(250)]
        public string Address { get; set; }

        [DataType(DataType.PhoneNumber,ErrorMessage ="Formata uygun bir telefon giriniz")]
        public string Phone { get; set; }

        [DataType(DataType.PhoneNumber, ErrorMessage = "Formata uygun bir fax giriniz")]
        public string Fax { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Formata uygun bir bir e-posta giriniz")]
        public string Email { get; set; }
    }
}